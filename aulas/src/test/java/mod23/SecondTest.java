package test.java.mod23;

import org.junit.Assert;
import org.junit.Test;

public class SecondTest {

    @Test
    public void test(){
        String nome = "Guilherme";
        Assert.assertEquals("Guilherme", nome);
    }

    @Test
    public void testNotEquals(){
        String nome = "Guilherme";
        Assert.assertNotEquals("Guilherme1", nome);
    }
}
