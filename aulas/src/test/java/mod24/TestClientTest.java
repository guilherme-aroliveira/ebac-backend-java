package test.java.mod24;

import main.java.mod24.TestClient;

import org.junit.Assert;
import org.junit.Test;


public class TestClientTest {

    @Test
    public void testClassClient() {
        TestClient cli = new TestClient();
        cli.adidionarNome("Guilherme");

        Assert.assertEquals("Guilherme", cli.getNome());
    }
}
