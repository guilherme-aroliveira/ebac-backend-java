package main.java.mod08;

public class Operadores {
    public static void main(String[] args)
    {
        operacoesAritmeticas();
        operacoesAtribuicoes();
        operacoesIncrementoDecremento();
        operadoresRelacionais();
        operadoresLogicos();
    }

    public static void operadoresLogicos()
    {
        System.out.println("**** operadoresLogicos ****");

        int num1 = 10;
        int num2 = 10;

        //1....10
        //boolean isDentro10 = num1 >= 1 && num1 <= 10;
        //System.out.println("isDentro10: " + isDentro10);

        //1....10
        //boolean isDentro10 = num1 >= 1 || num1 <= 9;
        //System.out.println("isDentro10: " + isDentro10);

        //1....10
        //boolean isNot = num1 >= 1;
        //System.out.println("isNot: " + !isNot);
    }

    public static void operadoresRelacionais()
    {
        System.out.println("**** operadoresRelacionais ****");

        int num1 = 10;
        int num2 = 10;
        boolean isMaior = false;
        System.out.println("isMaior: " + isMaior);

        boolean isMenor = false;
        System.out.println("isMenor: " + isMenor);

        boolean isDiff = false;
        System.out.println("isDiff: " + isDiff);

        boolean isMaiorIgual = true;
        System.out.println("isMaiorIgual: " + isMaiorIgual);
    }

    public static void operacoesIncrementoDecremento()
    {
        System.out.println("**** operacoesIncrementoDecremento ****");

        int num1= 10;
        System.out.println(num1);

        num1++;
        System.out.println(num1);

        num1--;
        System.out.println(num1);

    }

    public static void operacoesAtribuicoes()
    {
        System.out.println("**** operacoesAtribuicoes ****");
        int numero1 = 0;
        int numero2 = 10;
        int numero3 = numero1 + numero2;
        numero3 += numero3;
        int numero4 = numero3 + 20;
        System.out.println(numero3);
        System.out.println(numero4);

    }

    public static void operacoesAritmeticas()
    {
        int num1 = 10;
        int num2 = 20;

        int num3 = num1 + num2;
        int num4 = num1 - num2;
        int num5 = 0;
        int num6 = num1 * num2;
        int num7 = (10 + 10) / 2;


        System.out.println(num3);
        System.out.println(num4);
        System.out.println(num5);
        System.out.println(num6);
        System.out.println(num7);
    }
}
