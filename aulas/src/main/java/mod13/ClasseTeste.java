package main.java.mod13;

import main.java.mod13.interfaces.*;

public class ClasseTeste {
    public static void main(String[] args) {
        ICaneta caneta = new CanetaEsferografica();
        caneta.escrever("Olá Guilherme");
        caneta.escreverComumATodas();
        System.out.println(caneta.getCor());

        ICaneta giz = new Giz();
        giz.escrever("Olá Guilherme");
        caneta.escreverComumATodas();
        System.out.println(giz.getCor());

        Lapis lapis = new Lapis();
        giz.escrever("Olá Guilherme");
        caneta.escreverComumATodas();
        System.out.println(lapis.getCor());

        ICarro carro = new CarroPasseio();
        carro.andar();
        carro.parar();

        ICarro caminhao = new Caminhao();
        caminhao.andar();
        caminhao.parar();
    }
}
