package main.java.mod13.interfaces;

public class CarroPasseio implements ICarro {

    @Override
    public void andar() {
        System.out.println("Carro está andando rápido");
    }
}
