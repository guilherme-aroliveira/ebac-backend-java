package main.java.mod13.interfaces;

public class Caminhao implements ICarro {

    @Override
    public void andar() {
        System.out.println("Carro está andando devagar");
    }

    @Override //Sobrscrita do metodo em comum que esta na ubterface
    public void parar() {
        System.out.println("Caminhão parando");
    }
}

