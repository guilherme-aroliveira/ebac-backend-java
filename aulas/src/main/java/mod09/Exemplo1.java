package main.java.mod09;

public class Exemplo1
{
    public static void main(String[] args)
    {
        //Boxing
        Boolean status = true;
        //Boolean status1 = Boolean.True;

        Character c = 'A';

        Integer idade = 10;

        //Long cpf = 12312312l;

        //unboxing
        //boolean status2 = Boollean.True;
        //char letra = Character.valueOf('A');

        //int idade = Integer.valueOf(12);

        //long cpf3 = Long.valueOf(12313);
    }
}
