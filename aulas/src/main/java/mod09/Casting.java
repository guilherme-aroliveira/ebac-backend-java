package main.java.mod09;

public class Casting
{
    public static void main(String[] args)
    {
        //casting implicito
        short num1 = 10;

        //casting explicito
        short num2 = (short) num1;

        int idade = 10;
        long idadeL = idade;

        byte b = 1;
        short s = b;

        byte b1 = (byte) s;
        int i = b1;

        int i1 = 1;
        byte s1 = (byte) i1;

        long l1 = 101010;
        short l2 = (short) l1;
        double d = l1;

        double d1 = 1.0;
        long l3 = (long) d1;
    }
}
