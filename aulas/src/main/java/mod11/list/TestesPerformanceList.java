package main.java.mod11.list;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TestesPerformanceList
{
    public static void main(String[] args) {
        testeArrayListAdd();
        testeLinkedListAdd();

        testeArrayListRemove();
        testeLinkedListRemove();
    }

    private static void testeLinkedListRemove() {
        final int MAX = 20000;
        long iInicio = System.currentTimeMillis();

        List<Integer> lista = new LinkedList<Integer>();
        for (int i = 0; i < MAX; i++) {
            lista.add(i);
        }

        for (int i = MAX -1; i > 0; i--) {
            lista.remove(i);
        }
        long tFim = System.currentTimeMillis();
        System.out.println("Tempo total LinkedList Remove: " + (tFim - iInicio));
    }

    private static void testeLinkedListAdd() {
        final int MAX = 20000;
        long iInicio = System.currentTimeMillis();

        List<Integer> lista = new LinkedList<Integer>();
        for (int i = 0; i < MAX; i++) {
            lista.add(i);
        }
        for (int i = 0; i < MAX; i++) {
            lista.contains(i);
        }
        long tFim = System.currentTimeMillis();
        System.out.println("Tempo total LinkedList ADD: " + (tFim - iInicio));
    }

    private static void testeArrayListRemove() {
        final int MAX = 20000;
        long iInicio = System.currentTimeMillis();

        List<Integer> lista = new ArrayList<Integer>();
        for (int i = 0; i < MAX; i++) {
            lista.add(i);
        }
        for (int i = MAX -1; i > 0; i--) {
            lista.remove(i);
        }
        long tFim = System.currentTimeMillis();
        System.out.println("Tempo total ArrayList Remove: " + (tFim - iInicio));
    }

    private static void testeArrayListAdd() {
        final int MAX = 20000;
        long iInicio = System.currentTimeMillis();

        List<Integer> lista = new ArrayList<>();
        for (int i = 0; i < MAX; i++) {
            lista.add(i);
        }
        for (int i = 0; i < MAX; i++) {
            lista.contains(i);
        }
        long tFim = System.currentTimeMillis();
        System.out.println("Tempo total ArrayList ADD: " + (tFim - iInicio));
    }
}
