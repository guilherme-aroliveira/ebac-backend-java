package main.java.mod11.list;

import main.java.mod11.domain.Aluno;

import java.util.Comparator;

public class ComparaNotaAluno implements Comparator<Aluno> {

    @Override
    public int compare(Aluno o1, Aluno o2) {
        // return Double.compare(o2.getNota().compareTo(Double.valueOf(02.getNpta()));
        // return Double.compare(o2.getNota(), o1.getNota());
        return Double.compare(o1.getNota(), o2.getNota());
    }
}
