package main.java.mod11.list;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/*
 * Ao contrário do ArrayList, as buscas são lentas e inserções e exclusões são rápidas.
 *
 * @author guilherme.oliveira
 */
public class ExemploLinkedList
{
    public static void main(String[] args) {
        exemploListaSimples();
        exemploListaSimplesOrdemAscendente();
    }

    private static void exemploListaSimples() {
        System.out.println("******** exemploListaSimples *********");
        List<String> lista = new LinkedList<>();
        lista.add("João da Silva");
        lista.add("Antonio Souza");
        lista.add("Lúcio Ferreia");
        System.out.println(lista);
        System.out.println();

        lista.remove(0);
        boolean contem = lista.contains("Antonio Souza");
        System.out.println(lista);
        System.out.println(contem);

        for (String nome : lista) {
            System.out.println(nome);
        }

        System.out.println(lista.get(0));
    }

    private static void exemploListaSimplesOrdemAscendente() {
        System.out.println("******** exemploListaSimplesOrdemAscendente *********");
        List<String> lista = new LinkedList<>();
        lista.add("João da Silva");
        lista.add("Antonio Souza");
        lista.add("Lúcio Ferreia");
        Collections.sort(lista);
        System.out.println(lista);
        System.out.println();
    }
}
