package main.java.mod11.list;

import main.java.mod11.domain.Aluno;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExemploArrayListAluno
{
    public static void main(String[] args) {
        exemploListaSimplesOrdenadaComparatorAluno();
        exemploListaSimplesOrdenadaClasseExterna();
    }
    public static void exemploListaSimplesOrdenadaClasseExterna() {
        System.out.println("******** exemploListaSimplesOrdenadaClasseExterna *******");
        List<Aluno> lista = new ArrayList<Aluno>();

        Aluno a = new Aluno("João da Silva", "Linux básico", 0);
        Aluno b = new Aluno("Antonio Souza", "OPenOffice", 0);
        Aluno c = new Aluno("Lúcia Ferreira","Internet", 0);

        lista.add(a);
        lista.add(b);
        lista.add(c);
        System.out.println(lista);
        Collections.sort(lista);
        System.out.println(lista);
    }

    public static void exemploListaSimplesOrdenadaComparatorAluno(){
        System.out.println("******** exemploListaSimplesOrdenadaComparatorAluno *******");
        List<Aluno> lista = new ArrayList<Aluno>();

        Aluno a = new Aluno("João da Silva"," Linux básico ", 20 );
        Aluno b = new Aluno("Antonio Souza", "OPenOffice", 30);
        Aluno c = new Aluno("Lúcia Ferreira", "Internet", 10);

        lista.add(a);
        lista.add(b);
        lista.add(c);
        System.out.println("Lista sem ordencação" + lista);
        //Collections.sort(lista);
        System.out.println("Lista com ordencação" + lista);

        ComparaNotaAluno comparaNotaAluno = new ComparaNotaAluno();
        Collections.sort(lista, comparaNotaAluno);
        System.out.println("Lista com ordenação por nota" + lista);

        System.out.println("");
    }
}
